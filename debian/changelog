jasmin-sable (2.4.0-4) unstable; urgency=medium

  * Orphaning package.

 -- Chris Lamb <lamby@debian.org>  Sat, 08 Feb 2014 23:52:18 +0000

jasmin-sable (2.4.0-3) unstable; urgency=low

  * ``mkdir -p`` over ``mkdir``. (Closes: #711582)

 -- Chris Lamb <lamby@debian.org>  Mon, 10 Jun 2013 23:07:59 +0100

jasmin-sable (2.4.0-2) unstable; urgency=low

  * Update Vcs-{Browser,Git}.
  * Bump Standards-Version to 3.9.2.
  * Fix "Transition package to default java implementation" - apply patch from
    Ubuntu / James Page. Thanks to gregor herrmann <gregoa@debian.org>.
    (Closes: #683543)
  * Need to create "classes" dir.

 -- Chris Lamb <lamby@debian.org>  Wed, 22 May 2013 23:46:37 +0100

jasmin-sable (2.4.0-1) unstable; urgency=low

  * New upstream release.
  * Update debian/patches/01-ant-settings.diff to reflect upstream changes.
  * Bump Standards-Version to 3.9.1.
  * Move to '3.0 (quilt)' format.
  * Specify examples and manpages in debina/jasmin-sable.{manpages,examples}
    instead of as arguments in debian/rules.
  * Move to minimal debhelper 7 rules file.

 -- Chris Lamb <lamby@debian.org>  Sun, 01 Aug 2010 17:01:50 -0400

jasmin-sable (2.3.0-6) unstable; urgency=low

  * Don't specify a JAVA_HOME pointing to gcj; causes FTBFS when gcj is not
    installed. (Closes: #573675)

 -- Chris Lamb <lamby@debian.org>  Tue, 16 Mar 2010 10:08:40 +0000

jasmin-sable (2.3.0-5) unstable; urgency=low

  * Depend on java2-runtime instead of java-virtual-machine so we can be
    satisfied by openjdk-6-jre. (Closes: #572058)
  * Bump Standards-Version.
  * Build-Depend on openjdk-6-jdk instead of java-gcj-compat-dev.
  * Switch to dpkg-source 3.0 (quilt) format.

 -- Chris Lamb <lamby@debian.org>  Mon, 01 Mar 2010 17:43:56 +0000

jasmin-sable (2.3.0-4) unstable; urgency=low

  * Update 03-cup-11-support.diff to modify parser.cup instead of the generated
    parser.java.
  * Ensure parser.java is rebuilt from parser.cup.
  * Add 04-fix-field-support.diff to fix parsing of field definitions
    (Closes: #538861)

 -- Chris Lamb <lamby@debian.org>  Thu, 30 Jul 2009 16:09:08 +0200

jasmin-sable (2.3.0-3) unstable; urgency=low

  * Support most recent "cup" package (Closes: #538860, #538861)
    - Add 03-cup-0-11-support.diff to support newest CUP package.
    - Bump dependencies on this cup version.
  * Bump Standards-Version to 3.8.2.

 -- Chris Lamb <lamby@debian.org>  Mon, 27 Jul 2009 19:05:11 +0200

jasmin-sable (2.3.0-2) unstable; urgency=low

  * New maintainer email address.
  * Move ant to Build-Depends so it is satisfied in the clean target.
  * Bump debhelper compatibility level to 7; no changes.
  * Update location of Git repository.

 -- Chris Lamb <lamby@debian.org>  Mon, 16 Feb 2009 02:02:22 +0000

jasmin-sable (2.3.0-1) unstable; urgency=low

  * New upstream release.
  * Fix formatting error in jasmin(1) manpage.
  * Pass arguments to JVM using "$@" instead of $*.
  * Bump Standards-Version to 3.8.0.

 -- Chris Lamb <chris@chris-lamb.co.uk>  Tue, 15 Jul 2008 20:31:55 +0100

jasmin-sable (2.2.5-2) unstable; urgency=medium

  * Add missing Depends on 'cup' package (Closes: #477459)
  * Modify '02-jasmin-executable.diff' to ensure cup.jar is used when
    using executable wrapper.

 -- Chris Lamb <chris@chris-lamb.co.uk>  Wed, 23 Apr 2008 14:23:37 +0100

jasmin-sable (2.2.5-1) unstable; urgency=low

  * New upstream release.
    - Update '01-ant-settings.diff' patch

 -- Chris Lamb <chris@chris-lamb.co.uk>  Fri, 18 Apr 2008 16:25:56 +0100

jasmin-sable (2.2.4-1) unstable; urgency=low

  * New upstream release (Closes: #400603, #296926).
  * New maintainer.
  * Bump Standards-Version to 3.7.3.
  * Bump Debhelper compatibility version.
  * Add debian/watch.

  * debian/control:
    + Update Build-Depends and Depends to use java-gcj-compat-dev instead
      of sablevm.
    + Add Build-Depends on 'cup' LALR parser generator.
    + Update package description.
    + Add Homepage: field.
    + Add Vcs-* fields.

  * debian/rules:
    + Don't ignore clean target errors.
    + Remove useless dh_ calls.
    + Actually install examples.

  * debian/copyright:
    + Add all copyright holders to copyright notice.
    + Update FSF address.

  * debian/patches:
    + Move patches from Debian .diff.gz to Quilt.
    + Remove custom build system now that upstream provides Ant script.
    + Added patches to ensure:
      - Build system uses Debian version of CUP.
      - /usr/bin/jasmin uses package-provided jasmin-sable.jar.

  * debian/jasmin.1:
    + Rephrase wording and layout.
    + Fix Groff error related to character encoding.

 -- Chris Lamb <chris@chris-lamb.co.uk>  Fri, 08 Feb 2008 19:09:59 +0000

jasmin-sable (1.2-2) unstable; urgency=low

  * Larning to read bugreports with comprehension. Added 'fastjar' to
    Build-deps. Closes: #232987

 -- Grzegorz Prokopski (Debian Developer) <gadek@debian.org>  Sat,  3 Apr 2004 20:15:16 -0500

jasmin-sable (1.2-1) unstable; urgency=low

  * Initial Release. Lintian clean. Closes: #228843.

 -- Grzegorz Prokopski (Debian Developer) <gadek@debian.org>  Wed, 21 Jan 2004 03:33:28 -0500
